use anyhow;
use serde::{Deserialize, Serialize};
use std::{
    convert::{TryFrom, TryInto},
    error::Error,
    fmt::{write, Debug, Write},
    fs::File,
    str::FromStr,
    thread::panicking,
};
use std::{fmt::Display, num::ParseIntError};

#[derive(Default, Debug, PartialEq, Clone, Copy, Serialize, Deserialize)]
pub struct SemVer {
    major: u16,
    minor: u16,
    patch: u16,
}

impl FromStr for SemVer {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let components: Result<Vec<u16>, _> =
            s.split(".").map(|item| item.parse::<u16>()).collect();
        let components_vec = components?;
        let components_arr: [u16; 3] = components_vec
            .try_into()
            .map_err(|_| anyhow::anyhow!("wrong number of components"))?;

        Ok(SemVer::from(components_arr))
    }
}

impl From<[u16; 3]> for SemVer {
    fn from(components: [u16; 3]) -> Self {
        Self::new(components[0], components[1], components[2])
    }
}

impl Display for SemVer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "v{}.{}.{}",
            self.major, self.minor, self.patch
        ))
    }
}

impl SemVer {
    fn new(major: u16, minor: u16, patch: u16) -> Self {
        Self {
            major,
            minor,
            patch,
        }
    }

    fn new_simple(major: u16) -> Self {
        Self::new(major, u16::default(), u16::default())
    }
}

#[typetag::serde(tag = "type")]
pub trait Crate {
    fn name(&self) -> &str;
    fn release_history(&self) -> &Vec<SemVer>;
}

impl Display for dyn Crate {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.release_history().len() > 0 {
            write!(
                f,
                "{} {}",
                self.name(),
                self.release_history().first().unwrap()
            )
        } else {
            write!(f, "{}", self.name())
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CrateData {
    pub name: String,
    pub release_history: Vec<SemVer>,
}

#[typetag::serde]
impl Crate for CrateData {
    fn name(&self) -> &str {
        self.name.as_str()
    }

    fn release_history(&self) -> &Vec<SemVer> {
        &self.release_history
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum CrateEnum {
    Program(CrateData),
    Library(CrateData),
}

#[typetag::serde]
impl Crate for CrateEnum {
    fn name(&self) -> &str {
        match self {
            CrateEnum::Program(p) => p.name(),
            CrateEnum::Library(l) => l.name(),
        }
    }

    fn release_history(&self) -> &Vec<SemVer> {
        match self {
            CrateEnum::Program(p) => p.release_history(),
            CrateEnum::Library(l) => l.release_history(),
        }
    }
}

impl Display for CrateEnum {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let kind = match self {
            CrateEnum::Program(_) => "Program",
            CrateEnum::Library(_) => "Library",
        };

        if self.release_history().len() > 0 {
            write!(
                f,
                "{} '{}' {}",
                kind,
                self.name(),
                self.release_history().first().unwrap()
            )
        } else {
            write!(f, "{} '{}'", kind, self.name())
        }
    }
}

impl Display for Repository {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{} crates in repo", self.crates.len()))
            .unwrap();
        for c in self.crates.iter() {
            f.write_fmt(format_args!("\n\t{}", c)).unwrap();
        }
        Ok(())
    }
}

impl Display for DynRepository {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{} crates in dynrepo", self.crates.len()))
            .unwrap();
        for c in self.crates.iter() {
            f.write_fmt(format_args!("\n\t{}", c.as_ref())).unwrap();
        }
        Ok(())
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Repository {
    pub crates: Vec<CrateEnum>,
}

#[derive(Serialize, Deserialize)]
pub struct DynRepository {
    pub crates: Vec<Box<dyn Crate>>,
}
