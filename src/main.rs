use std::{
    fmt::{Debug, Display},
    str::FromStr,
};

use exo::{Crate, CrateData, CrateEnum, DynRepository, Repository, SemVer};

fn main() -> Result<(), anyhow::Error> {
    let program_crate = CrateEnum::Program(CrateData {
        name: "program".to_string(),
        release_history: vec![],
    });

    println!(
        "crate 1: {}",
        serde_json::to_string(&program_crate).unwrap()
    );

    let new_program: CrateEnum =
        serde_json::from_str("{\"Program\":{\"name\":\"another program\",\"release_history\":[{\"major\": 1, \"minor\": 3, \"patch\": 2}]}}")
            .unwrap();
    println!("de-serialized: {}", new_program);
    let lib_crate: CrateEnum =
        serde_json::from_str("{\"Library\":{\"name\":\"another library\",\"release_history\":[]}}")
            .unwrap();
    println!("de-serialized: {}", lib_crate);

    let sercrate = serde_json::to_string(&new_program).unwrap();
    println!("re-serialized: {}", sercrate);

    let repo = Repository {
        crates: vec![
            program_crate.clone(),
            new_program.clone(),
            lib_crate.clone(),
        ],
    };
    println!("repository: {}", serde_json::to_string(&repo).unwrap());

    let repo_str = "{\"crates\":
        [
            {\"Program\":
                {
                    \"name\":\"another program\",
                    \"release_history\":
                        [{\"major\": 1, \"minor\": 3, \"patch\": 2}]
                }
            },
            {\"Library\":
                {
                    \"name\":\"another library\",
                    \"release_history\":[]
                }
            }
        ]}";

    let ser_repo: Repository = serde_json::from_str(repo_str).unwrap();
    println!("de-serialized repo: {}", ser_repo);

    let dyn_repo = DynRepository {
        crates: vec![
            Box::new(program_crate),
            Box::new(new_program),
            Box::new(lib_crate),
        ],
    };
    println!("repository: {}", serde_json::to_string(&dyn_repo).unwrap());

    let repo_str = "{\"crates\":
        [
            {\"type\":\"CrateEnum\", \"Program\":
                {
                    \"name\":\"another program\",
                    \"release_history\":
                        [{\"major\": 1, \"minor\": 3, \"patch\": 2}]
                }
            },
            {\"type\":\"CrateEnum\", \"Library\":
                {
                    \"name\":\"another library\",
                    \"release_history\":[]
                }
            }
        ]}";

    let ser_dynrepo: DynRepository = serde_json::from_str(repo_str).unwrap();
    println!("de-serialized dynrepo: {}", ser_dynrepo);

    Ok(())
}
